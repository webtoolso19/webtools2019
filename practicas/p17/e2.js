function verificarCorreo(){
    var correoelec = document.getElementById('form-correo').value;

    var pos1 = correoelec.charAt(0);
    var iframedoc01 = verificador1.document;

    if(verificador1.contentDocument){
        iframedoc01 = verificador1.contentDocument;
    }else{
        if (verificador1.contentWindow){
            iframedoc01 = verificador1.contentWindow.document;
        }
    }

    if( (pos1=='1') || (pos1=='2') || (pos1=='3') || (pos1=='4') || (pos1=='5') || (pos1=='6') || (pos1=='7') || (pos1=='8') || (pos1=='9') || (pos1=='0') ){
        if(iframedoc01){
            iframedoc01.open();
            iframedoc01.writeln('El correo comienza con número');
            iframedoc01.close();
        }else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
    }else{
        if(iframedoc01){
            iframedoc01.open();
            iframedoc01.writeln('El correo comienza con letra o caracter');
            iframedoc01.close();
        }else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
    }

    var pos2 = correoelec.indexOf('@');
    var iframedoc02 = verificador2.document;

    if(verificador2.contentDocument){
        iframedoc02 = verificador2.contentDocument;
    }else{
        if (verificador2.contentWindow){
            iframedoc02 = verificador2.contentWindow.document;
        }
    }

    if ( pos2!=-1 )
	{
        if(iframedoc02){
            iframedoc02.open();
            iframedoc02.writeln('El correo contiene el carácter @');
            iframedoc02.close();
        }else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
    }
	else
	{
	    if(iframedoc02){
            iframedoc02.open();
            iframedoc02.writeln('El correro no contiene el carácter @');
            iframedoc02.close();
        }else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
    }
    
    var pos31 = correoelec.indexOf('.com');
    var pos32 = correoelec.indexOf('.mx');
    var pos33 = correoelec.indexOf('.net');
    var iframedoc03 = verificador3.document;

    if(verificador3.contentDocument){
        iframedoc03 = verificador3.contentDocument;
    }else{
        if (verificador3.contentWindow){
            iframedoc03 = verificador3.contentWindow.document;
        }
    }

    if ( pos31!=-1 )
	{
        if(iframedoc03){
            iframedoc03.open();
            iframedoc03.writeln('El correro termina con .com');
            iframedoc03.close();
        }else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
    }
	else
	{
	    if( pos32!=-1 ){
            if(iframedoc03){
                iframedoc03.open();
                iframedoc03.writeln('El correro termina con .mx');
                iframedoc03.close();
            }else{
                alert('No es posible insertar el contenido dinámicamente en el iframe.');
            }
        }else{
            if( pos33!=-1 ){
                if(iframedoc03){
                    iframedoc03.open();
                    iframedoc03.writeln('El correro termina con .net');
                    iframedoc03.close();
                }else{
                    alert('No es posible insertar el contenido dinámicamente en el iframe.');
                }
            }else{
                if(iframedoc03){
                    iframedoc03.open();
                    iframedoc03.writeln('No lleva .com, .mx o .net');
                    iframedoc03.close();
                }else{
                    alert('No es posible insertar el contenido dinámicamente en el iframe.');
                }
            }
        }
	}
}